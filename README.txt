Challenge Details:

Input a CSV file which begins with a 10 column headder containing [A, J]

Pass/Fail Conditions
	Each record needs to be verified to contain a data element in each of the 10 columns

Insert Data into a SQLite In-Memory database (I am assuming only good data)
Insert Bad data into bad-data-<timestamp>.csv
Create Log File for record #Recieved, #Successful, and #Failed stats



Approach:

After realizing there was more outliers than originally realized with double quotes I did a state machine for-loop to go over the chars in each record to detect when commas were in double quotes.
Initially, to minimize processing time I had believed the only outlier to be in the data URI and used a simple string.split.
I had assumed commas existing in the other fields would fail any further data validation but seeing as data validation is not part of the task I modified my approach to be more exact.


How To Run:
Program Requires a single argument which is the Absolute path of the Input .CSV file. The program will output a RESULTS folder in the same directory as the .CSV and create an in-memory SQLite DB

Open /src contents in Java IDE
Make sure /src/lib is included in classpath
Supply Program args via Run Config

Optional to compile a Jar file and run with .CSV Absolute path argument.
