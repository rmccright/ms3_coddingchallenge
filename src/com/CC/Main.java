package com.CC;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

public class Main {

    private static final String HEADER = "A,B,C,D,E,F,G,H,I,J";

    public static void main(String[] args) {
        //Check for proper number of args.
        if(args.length != 1){
            System.err.println("One Argument Accepted. " + args.length + " supplied.");
            System.exit(1);
        }
        //Check if arg is a .csv file.
        if(!args[0].endsWith(".csv")){
            System.err.println("Argument contains invalid CSV extension.");
            System.exit(1);
        }

        parseCSV(args[0]);
    }

    public static void parseCSV(String csvFile){

        //Create vars for individual row, validation result, record count, and validation pas/fail tally
        String csvRow;
        boolean isRecordValid;
        int total = 0;
        int numGood = 0;
        int numBad = 0;
        List<String> recordElements;

        //Create In-Memory SQL database (In-Memory Databases cease to exist once connection is closed)
        SQLite_Util sqLite = new SQLite_Util(":memory:");
        //Create [A, J] table in the DB
        sqLite.createTable();

        //Create a Results Folder for the bad-data-<timeStamp>.csv and the stats.log
        String resultsPath = csvFile.substring(0, csvFile.lastIndexOf('.')) + "_RESULTS\\";
        createResultsFolder(resultsPath);
        File badData = new File(resultsPath + "bad-data-" + new SimpleDateFormat("MM_dd_yyyy_HH_mm'.txt'").format(new Date()) + ".csv");


        //Read Input CSV line by line -> check validation -> then put Record into bad-data csv or good data SQLite DB based on validation result
        try(FileWriter fileWriter = new FileWriter(badData) ) {
            try (Scanner scanner = new Scanner(new File(csvFile))) {
                scanner.useDelimiter(",");

                //Check if proper header exists -> remove it
                if (scanner.hasNextLine() && !scanner.nextLine().equals(HEADER)) {
                    System.err.println("Invalid .csv Column Header");
                    System.exit(1);
                }
                fileWriter.write(HEADER + '\n');

                while (scanner.hasNextLine()) {
                    csvRow = scanner.nextLine();

                    if(csvRow.length() > 0){

                        recordElements = extractRecordValues(csvRow);
                        isRecordValid = validateRecord(recordElements);

                        if(!isRecordValid)
                            fileWriter.write(csvRow + '\n');
                        else
                            sqLite.insertRecord(recordElements);

                        //Tally Records Stats based on Validation
                        if (isRecordValid) {
                            numGood++;
                        } else {
                            numBad++;
                        }
                        total++;
                    }

                }
                //Close the DB
                sqLite.closeDB();

            } catch (FileNotFoundException e){
                System.err.println("CSV File with path: " + csvFile + " Not Found.");
                System.exit(1);
            }
        } catch (IOException e){
            System.err.println("Error writing bad-data-<timestamp>.csv with exception: " + e);
            System.exit(1);
        }

        //Create Log File
        File statsLog = new File(resultsPath + "stats" + ".log");
        try(FileWriter fileWriter = new FileWriter(statsLog) ) {
            fileWriter.write("# of Records Received:" + total + '\n');
            fileWriter.write("# of Records Successful:" + numGood + '\n');
            fileWriter.write("# of Records Failed:" + numBad + '\n');
        } catch (IOException e){
            System.err.println("Error writing stats.log with exception: " + e);
            System.exit(1);
        }

    }

    public static void createResultsFolder(String path){
        File dir = new File(path);
        //Recursively Delete Existing Results
        deleteDirectory(dir);

        //Create Results DIR
        boolean successful = dir.mkdir();
        if (!successful)
        {
            System.err.println("Error writing creating results dir. Likely caused by deleted folder being open in another program such as File Explorer");
            System.exit(1);
        }
    }

    public static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }

    public static List<String> extractRecordValues(String row) {
        //Initialize Quote Tracking to false (outside quotes currently)
        boolean inDoubleQuotes = false;
        //keep track of the beginning of current element
        int elemStart = 0;
        //List to store elements
        List<String> recordElements = new ArrayList<>();

        //Loop through each char in string
        for(int i=0; i<row.length(); i++){
            //If current char is a double quote toggle quote status and jump to next char
            if(row.charAt(i) == '"') {
                inDoubleQuotes = !inDoubleQuotes;
                continue;
            }

            //We only care about commas Not inDoubleQuotes
            if(!inDoubleQuotes && row.charAt(i) == ','){
                recordElements.add(row.substring(elemStart, i));
                elemStart = i + 1; //+1 to ignore actual comma
            }
        }
        //Grab last substring element
        recordElements.add(row.substring(elemStart));

        return recordElements;
    }


    public static boolean validateRecord(List<String> recordElements){
        return recordElements.size() == 10;
    }
}
