package com.CC;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class SQLite_Util {

    private Connection c = null;

    public SQLite_Util(String location){
        try {
            Class.forName("org.sqlite.JDBC");
            this.c = DriverManager.getConnection("jdbc:sqlite:" + location);
        } catch (Exception e) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(1);
        }
    }

    public void createTable(){

        try(Statement stmt = this.c.createStatement()){
            String sql = "CREATE TABLE Successful_Data " +
                    "(A TEXT NOT NULL, " +
                    " B TEXT NOT NULL, " +
                    " C TEXT NOT NULL, " +
                    " D TEXT NOT NULL, " +
                    " E TEXT NOT NULL, " +
                    " F TEXT NOT NULL, " +
                    " G TEXT NOT NULL, " +
                    " H TEXT NOT NULL, " +
                    " I TEXT NOT NULL, " +
                    " J TEXT NOT NULL)";

            stmt.executeUpdate(sql);
        }catch(SQLException e){
            System.err.println("Error creating SQL Table: " + e);
            System.exit(1);
        }
    }

    public void insertRecord(List<String> recordElements){
        String sql = "INSERT INTO Successful_Data (A,B,C,D,E,F,G,H,I,J) VALUES (?,?,?,?,?,?,?,?,?,?)";

        try(PreparedStatement pstmt = this.c.prepareStatement(sql)){
            for(int i = 0; i < recordElements.size(); i++){
                pstmt.setString(i + 1, recordElements.get(i));
            }
            pstmt.executeUpdate();
        }catch(SQLException e){
            System.err.println("Error inserting record: " + e);
            System.exit(1);
        }
    }

    public void closeDB(){
        try{c.close();
        }catch(SQLException e){
            System.err.println("Error Closing SQLite DB: " + e);
            System.exit(1);
        }
    }


}
